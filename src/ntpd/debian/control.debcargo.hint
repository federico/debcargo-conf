Source: rust-ntpd
Section: utils
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native,
 rustc:native (>= 1.67),
 libstd-rust-dev,
 librust-async-trait-0.1+default-dev (>= 0.1.22-~~),
 librust-libc-0.2+default-dev (>= 0.2.145-~~),
 librust-ntp-os-clock-1+default-dev (>= 1.1.1-~~),
 librust-ntp-proto-1+--internal-api-dev (>= 1.1.1-~~),
 librust-ntp-proto-1+default-dev (>= 1.1.1-~~),
 librust-ntp-udp-1+default-dev (>= 1.1.1-~~),
 librust-rand-0.8+default-dev,
 librust-rustls-0.21+default-dev,
 librust-rustls-native-certs-0.6+default-dev,
 librust-rustls-pemfile-1+default-dev,
 librust-serde-1+default-dev (>= 1.0.145-~~),
 librust-serde-1+derive-dev (>= 1.0.145-~~),
 librust-serde-json-1+default-dev,
 librust-thiserror-1+default-dev (>= 1.0.10-~~),
 librust-tokio-1+default-dev (>= 1.32-~~),
 librust-tokio-1+fs-dev (>= 1.32-~~),
 librust-tokio-1+io-std-dev (>= 1.32-~~),
 librust-tokio-1+io-util-dev (>= 1.32-~~),
 librust-tokio-1+macros-dev (>= 1.32-~~),
 librust-tokio-1+net-dev (>= 1.32-~~),
 librust-tokio-1+rt-multi-thread-dev (>= 1.32-~~),
 librust-tokio-1+sync-dev (>= 1.32-~~),
 librust-toml-0.8+default-dev | librust-toml-0.7+default-dev | librust-toml-0.6+default-dev | librust-toml-0.5+default-dev,
 librust-tracing-0.1+default-dev (>= 0.1.21-~~),
 librust-tracing-subscriber-0.3+ansi-dev,
 librust-tracing-subscriber-0.3+fmt-dev,
 librust-tracing-subscriber-0.3+std-dev,
 help2man
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/ntpd]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/ntpd
Homepage: https://github.com/pendulum-project/ntpd-rs
X-Cargo-Crate: ntpd
Rules-Requires-Root: no

Package: librust-ntpd-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-async-trait-0.1+default-dev (>= 0.1.22-~~),
 librust-libc-0.2+default-dev (>= 0.2.145-~~),
 librust-ntp-os-clock-1+default-dev (>= 1.1.1-~~),
 librust-ntp-proto-1+--internal-api-dev (>= 1.1.1-~~),
 librust-ntp-proto-1+default-dev (>= 1.1.1-~~),
 librust-ntp-proto-1+ntpv5-dev (>= 1.1.1-~~),
 librust-ntp-proto-1+nts-pool-dev (>= 1.1.1-~~),
 librust-ntp-udp-1+default-dev (>= 1.1.1-~~),
 librust-rand-0.8+default-dev,
 librust-rustls-0.21+default-dev,
 librust-rustls-native-certs-0.6+default-dev,
 librust-rustls-pemfile-1+default-dev,
 librust-serde-1+default-dev (>= 1.0.145-~~),
 librust-serde-1+derive-dev (>= 1.0.145-~~),
 librust-serde-json-1+default-dev,
 librust-thiserror-1+default-dev (>= 1.0.10-~~),
 librust-tokio-1+default-dev (>= 1.32-~~),
 librust-tokio-1+fs-dev (>= 1.32-~~),
 librust-tokio-1+io-std-dev (>= 1.32-~~),
 librust-tokio-1+io-util-dev (>= 1.32-~~),
 librust-tokio-1+macros-dev (>= 1.32-~~),
 librust-tokio-1+net-dev (>= 1.32-~~),
 librust-tokio-1+rt-multi-thread-dev (>= 1.32-~~),
 librust-tokio-1+sync-dev (>= 1.32-~~),
 librust-toml-0.8+default-dev | librust-toml-0.7+default-dev | librust-toml-0.6+default-dev | librust-toml-0.5+default-dev,
 librust-tracing-0.1+default-dev (>= 0.1.21-~~),
 librust-tracing-subscriber-0.3+ansi-dev,
 librust-tracing-subscriber-0.3+fmt-dev,
 librust-tracing-subscriber-0.3+std-dev
Provides:
 librust-ntpd+--internal-fuzz-dev (= ${binary:Version}),
 librust-ntpd+default-dev (= ${binary:Version}),
 librust-ntpd+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd+unstable-nts-pool-dev (= ${binary:Version}),
 librust-ntpd-1-dev (= ${binary:Version}),
 librust-ntpd-1+--internal-fuzz-dev (= ${binary:Version}),
 librust-ntpd-1+default-dev (= ${binary:Version}),
 librust-ntpd-1+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd-1+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd-1+unstable-nts-pool-dev (= ${binary:Version}),
 librust-ntpd-1.1-dev (= ${binary:Version}),
 librust-ntpd-1.1+--internal-fuzz-dev (= ${binary:Version}),
 librust-ntpd-1.1+default-dev (= ${binary:Version}),
 librust-ntpd-1.1+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd-1.1+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd-1.1+unstable-nts-pool-dev (= ${binary:Version}),
 librust-ntpd-1.1.1-dev (= ${binary:Version}),
 librust-ntpd-1.1.1+--internal-fuzz-dev (= ${binary:Version}),
 librust-ntpd-1.1.1+default-dev (= ${binary:Version}),
 librust-ntpd-1.1.1+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd-1.1.1+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd-1.1.1+unstable-nts-pool-dev (= ${binary:Version})
Description: Full-featured implementation of NTP with NTS support - Rust source code
 Source code for Debianized Rust crate "ntpd"

Package: ntpd
Architecture: any
Multi-Arch: allowed
Section: utils
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends},
 adduser
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: Rust-based NTP implementation with NTS support
 ntpd-rs is an NTP implementation written in Rust,
 emphasizing security and stability.
 It provides both client and server functionalities and
 supports NTS.
