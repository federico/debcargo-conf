Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wayland-protocols
Upstream-Contact: Elinor Berger <elinor@safaradeg.net>
Source: https://github.com/smithay/wayland-rs

Files: *
Copyright: 2015-2023 Elinor Berger <elinor@safaradeg.net>
License: MIT

Files: protocols/stable/presentation-time/presentation-time.xml
Copyright: 2013-2014 Collabora, Ltd.
License: MIT

Files: protocols/stable/viewporter/viewporter.xml
Copyright: 2013-2016 Collabora, Ltd.
License: MIT 

Files: protocols/stable/xdg-shell/xdg-shell.xml
Copyright:
 2008-2013 Kristian Høgsberg
 2013      Rafael Antognolli
 2013      Jasper St. Pierre
 2010-2013 Intel Corporation
 2015-2017 Samsung Electronics Co., Ltd
 2015-2017 Red Hat Inc.
License: MIT

Files: protocols/staging/content-type/content-type-v1.xml
Copyright:
 2021 Emmanuel Gil Peyrot
 2022 Xaver Hugl
License: MIT 

Files: protocols/staging/cursor-shape/cursor-shape-v1.xml
Copyright:
 2018 The Chromium Authors
 2023 Simon Ser
License: MIT

Files: protocols/staging/drm-lease/drm-lease-v1.xml
Copyright:
 2018 NXP
 2019 Status Research &amp; Development GmbH.
 2021 Xaver Hugl
License: MIT 

Files: protocols/staging/ext-foreign-toplevel-list/ext-foreign-toplevel-list-v1.xml
Copyright:
 2018 Ilia Bozhinov
 2020 Isaac Freund
 2022 wb9688
 2023 i509VCB
License: MIT

Files: protocols/staging/ext-idle-notify/ext-idle-notify-v1.xml
Copyright:
 2015 Martin Gräßlin
 2022 Simon Ser
License: MIT 

Files: protocols/staging/ext-session-lock/ext-session-lock-v1.xml
Copyright: 2021 Isaac Freund
License: MIT 

Files: protocols/staging/fractional-scale/fractional-scale-v1.xml
Copyright: 2022 Kenny Levinsen
License: MIT 

Files: protocols/staging/security-context/security-context-v1.xml
Copyright: 2021 Simon Ser
License: MIT

Files: protocols/staging/single-pixel-buffer/single-pixel-buffer-v1.xml
Copyright: 2022 Simon Ser
License: MIT 

Files: protocols/staging/tearing-control/tearing-control-v1.xml
Copyright: 2021 Xaver Hugl
License: MIT 

Files: protocols/staging/xdg-activation/xdg-activation-v1.xml
Copyright:
 2020 Aleix Pol Gonzalez &lt;aleixpol@kde.org&gt;
 2020 Carlos Garnacho &lt;carlosg@gnome.org&gt;
License: MIT

Files: protocols/staging/xwayland-shell/xwayland-shell-v1.xml
Copyright: 2022 Joshua Ashton
License: MIT 

Files: protocols/unstable/fullscreen-shell/fullscreen-shell-unstable-v1.xml
Copyright:
 2016 Yong Bakos
 2015 Jason Ekstrand
 2015 Jonas Ådahl
License: MIT 

Files: protocols/unstable/idle-inhibit/idle-inhibit-unstable-v1.xml
Copyright: 2015 Samsung Electronics Co., Ltd
License: MIT 

Files: protocols/unstable/input-method/input-method-unstable-v1.xml
Copyright: 2012, 2013 Intel Corporation
License: MIT 

Files: protocols/unstable/input-timestamps/input-timestamps-unstable-v1.xml
Copyright: 2017 Collabora, Ltd.
License: MIT 

Files: protocols/unstable/keyboard-shortcuts-inhibit/keyboard-shortcuts-inhibit-unstable-v1.xml
Copyright: 2017 Red Hat Inc.
License: MIT

Files: protocols/unstable/linux-dmabuf/feedback.rst
Copyright: 2021 Simon Ser
License: MIT 

Files: protocols/unstable/linux-dmabuf/linux-dmabuf-unstable-v1.xml
Copyright: 2014, 2015 Collabora, Ltd.
License: MIT 

Files: protocols/unstable/linux-explicit-synchronization/linux-explicit-synchronization-unstable-v1.xml
Copyright:
 2016 The Chromium Authors.
 2017 Intel Corporation
 2018 Collabora, Ltd
License: MIT

Files: protocols/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml
Copyright:
 2014      Jonas Ådahl
 2015      Red Hat Inc.
License: MIT

Files: protocols/unstable/primary-selection/primary-selection-unstable-v1.xml
Copyright: 2015, 2016 Red Hat
License: MIT

Files: protocols/unstable/relative-pointer/relative-pointer-unstable-v1.xml
Copyright:
 2014      Jonas Ådahl
 2015      Red Hat Inc.
License: MIT

Files: protocols/unstable/tablet/tablet-unstable-v1.xml
Copyright:
 2014 © Stephen "Lyude" Chandler Paul
 2015-2016 © Red Hat, Inc.
License: MIT

Files: protocols/unstable/tablet/tablet-unstable-v2.xml
Copyright:
 2014 © Stephen "Lyude" Chandler Paul
 2015-2016 © Red Hat, Inc.
License: MIT

Files: protocols/unstable/text-input/text-input-unstable-v1.xml
Copyright: 2012, 2013 Intel Corporation
License: MIT

Files: protocols/unstable/text-input/text-input-unstable-v3.xml
Copyright:
 2012, 2013 Intel Corporation
 2015, 2016 Jan Arne Petersen
 2017, 2018 Red Hat, Inc.
 2018       Purism SPC
License: MIT

Files: protocols/unstable/xdg-decoration/xdg-decoration-unstable-v1.xml
Copyright: 2018 Simon Ser
License: MIT

Files: protocols/unstable/xdg-foreign/xdg-foreign-unstable-v1.xml
Copyright: 2015-2016 Red Hat Inc.
License: MIT

Files: protocols/unstable/xdg-foreign/xdg-foreign-unstable-v2.xml
Copyright: 2015-2016 Red Hat Inc.
License: MIT

Files: protocols/unstable/xdg-output/xdg-output-unstable-v1.xml
Copyright: 2017 Red Hat Inc.
License: MIT

Files: protocols/unstable/xdg-shell/xdg-shell-unstable-v5.xml
Copyright:
 2008-2013 Kristian Høgsberg
 2013      Rafael Antognolli
 2013      Jasper St. Pierre
 2010-2013 Intel Corporation
License: MIT

Files: protocols/unstable/xdg-shell/xdg-shell-unstable-v6.xml
Copyright:
 2008-2013 Kristian Høgsberg
 2013      Rafael Antognolli
 2013      Jasper St. Pierre
 2010-2013 Intel Corporation
License: MIT

Files: protocols/unstable/xwayland-keyboard-grab/xwayland-keyboard-grab-unstable-v1.xml
Copyright: 2017 Red Hat Inc.
License: MIT

Files: debian/*
Copyright:
 2022 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2022 Alexander Kjäll <alexander.kjall@gmail.com>
License: MIT MIT

License: MIT MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: MIT LGPL-2.1
 Debian systems provide the LGPL 2.1 in /usr/share/common-licenses/LGPL-2.1

License: MIT HPND-sell
 Permission to use, copy, modify, distribute, and sell this
 software and its documentation for any purpose is hereby granted
 without fee, provided that the above copyright notice appear in
 all copies and that both that copyright notice and this permission
 notice appear in supporting documentation, and that the name of
 the copyright holders not be used in advertising or publicity
 pertaining to distribution of the software without specific,
 written prior permission.  The copyright holders make no
 representations about the suitability of this software for any
 purpose.  It is provided "as is" without express or implied
 warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 THIS SOFTWARE.
