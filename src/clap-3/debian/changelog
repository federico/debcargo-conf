rust-clap-3 (3.2.25-3) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.25 from crates.io using debcargo 2.6.1
  * Relax dependency on terminal_size.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 25 Jan 2024 06:01:06 +0000

rust-clap-3 (3.2.25-2) experimental; urgency=medium

  * Team upload.
  * Package clap 3.2.25 from crates.io using debcargo 2.6.1
  * Add patch for clap-lex 0.6.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 13 Jan 2024 17:59:25 +0000

rust-clap-3 (3.2.25-1) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.25 from crates.io using debcargo 2.6.0
  * Add patches for clap-lex 0.5 (backported from clap 4.x packaging)

 -- Peter Michael Green <plugwash@debian.org>  Thu, 06 Jul 2023 01:48:10 +0000

rust-clap-3 (3.2.23-4) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.23 from crates.io using debcargo 2.6.0
  * Restore derive feature.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 12 Jan 2023 11:51:27 +0000

rust-clap-3 (3.2.23-3) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.23 from crates.io using debcargo 2.6.0
  * Bump dependency on clap-lex.
  * Disable derive feature for now, it should be re-enabled when
    rust-clap-derive-3 clears new.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 11 Jan 2023 00:49:48 +0000

rust-clap-3 (3.2.23-2) unstable; urgency=medium

  * Package clap 3.2.23 from crates.io using debcargo 2.5.0
  * Package clap 3 as a new package as clap 4 is here

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 05 Nov 2022 22:05:55 +0100

rust-clap (3.2.23-1) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.23 from crates.io using debcargo 2.5.0
  * Update dep relax patch with upper bounds due to their current absence in
    the archive

 -- Blair Noctis <n@sail.ng>  Sun, 30 Oct 2022 09:57:10 +0100

rust-clap (3.2.22-2) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.22 from crates.io using debcargo 2.5.0
  * Allow building against strsim 0.10

 -- James McCoy <jamessan@debian.org>  Sat, 08 Oct 2022 18:36:42 -0400

rust-clap (3.2.22-1) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.22 from crates.io using debcargo 2.5.0
  * Revert upstream bump of textwrap and terminal_size dependencies.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 08 Oct 2022 07:49:46 +0000

rust-clap (3.2.16-1) unstable; urgency=medium

  * Team upload.
  * Package clap 3.2.16 from crates.io using debcargo 2.5.0 (Closes: 1016409)
  * Drop empty patch no-clippy.patch
  * Drop disable-criterion.patch, rust-criterion is now in Debian.
  * Update remaining patches for new upstream
  * Disable unstable-v4 feature to avoid adding a new binary package.
  * Remove dev-dependency on snapbox, it's not in Debian and the tests that use it do not appear
    to be included in the crates.io release.
  * Remove dependency of "deprecated" feature on "clap_derive/deprecated", this
    avoids the generation of an additional binary package, and the "deprecated"
    feature is only used to enable additional deprecation warnings so it's not
    super-important.
  * Make once-cell dependency non optional to avoid generating an aditional
    binary package.
  * Prevent generatoin of "clap" binary package that appeared with the update to
    the new upstream version. The only binary that ends up in it appears to be
    a trivial test program.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 01 Aug 2022 00:20:30 +0000

rust-clap (3.1.8-1) unstable; urgency=medium

  * Team upload.
  * Package clap 3.1.8 from crates.io using debcargo 2.5.0
    + Versions 3.1.9 and above require clap-lex which is not currntly in Debian.
  * Disable unstable-doc feature, it does not seem relavent for Debian
    (it seems to be intended only for building docs.rs documentation).
  * Update copyright file.

  [ Ben Westover ]
  * Package clap 3.1.6 from crates.io using debcargo 2.5.0
  * Re-enable derive feature now that rust-clap-derive is packaged (Closes: #1011193)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 07 Jun 2022 19:48:19 +0000

rust-clap (3.1.6-2) unstable; urgency=medium

  * Team upload.
  * Package clap 3.1.6 from crates.io using debcargo 2.5.0

 -- Peter Michael Green <plugwash@debian.org>  Sun, 27 Mar 2022 00:31:03 +0000

rust-clap (3.1.6-1) unstable; urgency=medium

  * Team upload.
  * Package clap 3.1.6 from crates.io using debcargo 2.5.0
  * Remove dev-dependency on criterion and disable benches that rely
    on it.
  * Remove dev-dependency on trycmd, the tests that use it are not
    in the crates.io release of the crate anyway.
  * Disable derive feature, rust-clap-derive is not currently in
    Debian.
  * Establish baseline for tests.

  [ Sylvestre Ledru ]
  * Package clap 3.1.6 from crates.io using debcargo 2.5.0

 -- Peter Michael Green <plugwash@debian.org>  Fri, 25 Mar 2022 17:32:28 +0100

rust-clap (3.1.1-1) UNRELEASED; urgency=medium

  * Package clap 3.1.1 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 23 Feb 2022 14:59:59 +0100

rust-clap (2.33.3-1) unstable; urgency=medium

  * Team upload.
  * Package clap 2.33.3 from crates.io using debcargo 2.4.3

 -- Stephan Lachnit <stephanlachnit@protonmail.com>  Sun, 01 Nov 2020 12:42:15 +0100

rust-clap (2.33.0-5) unstable; urgency=medium

  * Team upload.
  * Update clap patch for structopt

 -- kpcyrd <git@rxv.cc>  Wed, 23 Oct 2019 23:45:32 +0200

rust-clap (2.33.0-4) unstable; urgency=medium

  * Team upload.
  * Package clap 2.33.0 from crates.io using debcargo 2.4.0
  * Update ansi_term to 0.12

 -- Ximin Luo <infinity0@debian.org>  Wed, 28 Aug 2019 07:02:18 -0700

rust-clap (2.33.0-3) unstable; urgency=medium

  * Team upload.
  * Package clap 2.33.0 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Tue, 16 Jul 2019 23:18:47 -0700

rust-clap (2.33.0-2) unstable; urgency=medium

  * Team upload.
  * Package clap 2.33.0 from crates.io using debcargo 2.2.10
  * Forcibly use strsim 0.7 as 0.8 isn't ready yet in Debian.

 -- Ximin Luo <infinity0@debian.org>  Fri, 31 May 2019 23:19:49 -0700

rust-clap (2.33.0-1) unstable; urgency=medium

  * Team upload.
  * Package clap 2.33.0 from crates.io using debcargo 2.2.10

 -- Ximin Luo <infinity0@debian.org>  Thu, 30 May 2019 23:21:22 -0700

rust-clap (2.32.0-4) unstable; urgency=medium

  * Team upload.
  * Package clap 2.32.0 from crates.io using debcargo 2.2.9
  * Allow building against yaml-rust 0.4.

 -- Ximin Luo <infinity0@debian.org>  Wed, 26 Dec 2018 13:39:47 -0800

rust-clap (2.32.0-3) unstable; urgency=medium

  * Team upload.
  * Package clap 2.32.0 from crates.io using debcargo 2.2.9
  * Allow building against textwrap 0.11.

 -- Ximin Luo <infinity0@debian.org>  Tue, 18 Dec 2018 19:12:05 -0800

rust-clap (2.32.0-2) unstable; urgency=medium

  * Package clap 2.32.0 from crates.io using debcargo 2.2.6
  * Avoid dependency on clippy, it's only used by devs for linting.

 -- Ximin Luo <infinity0@debian.org>  Fri, 07 Sep 2018 20:48:43 -0700

rust-clap (2.32.0-1) unstable; urgency=medium

  * Package clap 2.32.0 from crates.io using debcargo 2.2.5

  [ Sylvestre Ledru ]
  * Package clap 2.32.0 from crates.io using debcargo 2.1.4

 -- Ximin Luo <infinity0@debian.org>  Wed, 01 Aug 2018 19:45:39 -0700
