Source: rust-foreign-types
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 18),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-foreign-types-macros-0.2+default-dev <!nocheck>,
 librust-foreign-types-macros-0.2+std-dev <!nocheck>,
 librust-foreign-types-shared-0.3+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Ximin Luo <infinity0@debian.org>,
 Fabio Rafael da Rosa <fdr@fabiodarosa.org>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/foreign-types]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/foreign-types

Package: librust-foreign-types-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-foreign-types-macros-0.2+default-dev,
 librust-foreign-types-shared-0.3+default-dev
Recommends:
 librust-foreign-types+std-dev (= ${binary:Version})
Provides:
 librust-foreign-types-0-dev (= ${binary:Version}),
 librust-foreign-types-0.5-dev (= ${binary:Version}),
 librust-foreign-types-0.5.0-dev (= ${binary:Version})
Description: Framework for Rust wrappers over C APIs - Rust source code
 Rust crate foreign-types

Package: librust-foreign-types+std-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-foreign-types-dev (= ${binary:Version}),
 librust-foreign-types-macros-0.2+std-dev
Provides:
 librust-foreign-types+default-dev (= ${binary:Version}),
 librust-foreign-types-0+std-dev (= ${binary:Version}),
 librust-foreign-types-0+default-dev (= ${binary:Version}),
 librust-foreign-types-0.5+std-dev (= ${binary:Version}),
 librust-foreign-types-0.5+default-dev (= ${binary:Version}),
 librust-foreign-types-0.5.0+std-dev (= ${binary:Version}),
 librust-foreign-types-0.5.0+default-dev (= ${binary:Version})
Description: Framework for Rust wrappers over C APIs - feature "std" and 1 more
 This metapackage enables feature "std" for the Rust foreign-types crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "default" feature.
